package config

import (
	"github.com/shomali11/slacker"
	"os"
)

func ConnectionSlack() *slacker.Slacker {
	token, ok := os.LookupEnv("SLACK_TOKEN")
	if !ok {
		panic("failed load env")
	}
	bot := slacker.NewClient(token)
	return bot
}
