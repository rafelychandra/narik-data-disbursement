package config

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"os"
)

func ConnectionDatabase() (*gorm.DB, error) {
	host, ok := os.LookupEnv("HOST_DB")
	if !ok {
		panic("failed load env")
	}
	port, ok := os.LookupEnv("PORT_DB")
	if !ok {
		panic("failed load env")
	}
	user, ok := os.LookupEnv("USER_DB")
	if !ok {
		panic("failed load env")
	}
	database, ok := os.LookupEnv("DATABASE_NAME")
	if !ok {
		panic("failed load env")
	}
	password, ok := os.LookupEnv("PASSWORD")
	if !ok {
		panic("failed load env")
	}

	connection := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s", host, port, user, database, password)

	db, err := gorm.Open("postgres", connection)
	if err != nil {
		return nil, err
	}
	return db, nil
}
