package modules

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/shomali11/slacker"
	"github.com/slack-go/slack"
	"io/ioutil"
	"narikData/helpers"
	"time"
)

func ProcessDataDid(db *gorm.DB, disbursementDay uint64) string {
	dataDid, err := GetDataDid(db, disbursementDay)
	if err != nil {
		return ""
	}
	payloadCSV := helpers.ConvertToArrayString(dataDid)

	file, err := helpers.CreateCSV(payloadCSV)
	if err != nil {
		return ""
	}
	contentCSV, err := ioutil.ReadFile(file.Name())
	if err != nil {
		return ""
	}
	return string(contentCSV)
}

func ProcessDataNotDid(db *gorm.DB, disbursementDay uint64) string {
	dataDid, err := GetDataNotDid(db, disbursementDay)
	if err != nil {
		return ""
	}
	payloadCSV := helpers.ConvertToArrayString(dataDid)

	file, err := helpers.CreateCSV(payloadCSV)
	if err != nil {
		return ""
	}
	contentCSV, err := ioutil.ReadFile(file.Name())
	if err != nil {
		return ""
	}
	return string(contentCSV)
}

func CallBot(db *gorm.DB) (*slacker.CommandDefinition, error) {
	var fileName, content, announcement string
	disbursementDay := uint64(1)
	date := time.Now().Add(24 * time.Hour).Format("2006_01_02")
	if int(time.Now().Weekday()) == 5 {
		date = time.Now().Add(72 * time.Hour).Format("2006_01_02")
		disbursementDay = 3
	}

	timeNow := time.Now().Format("2006-01-02")
	data := &slacker.CommandDefinition{
		Description: "Get Data Disbursement",
		Handler: func(botCtx slacker.BotContext, request slacker.Request, response slacker.ResponseWriter) {

			channel := botCtx.Event().Channel
			rtm := botCtx.RTM()
			client := botCtx.Client()

			payload := request.Param("payload")
			datePayload := request.Param("time")

			if payload == "did" {
				if datePayload != "" {
					announcement = fmt.Sprintf("getting data disbursement did %s", datePayload)
					fileName = fmt.Sprintf("dis_%s_did.csv", datePayload)
					diff := helpers.DaysBetween(helpers.Date(timeNow), helpers.Date(datePayload))
					content = ProcessDataDid(db, diff)
				} else {
					announcement = fmt.Sprintf("getting data disbursement did %s", date)
					fileName = fmt.Sprintf("dis_%s_did.csv", date)
					content = ProcessDataDid(db, disbursementDay)
				}
			} else if payload == "notdid" {
				if datePayload != "" {
					announcement = fmt.Sprintf("getting data disbursement %s", datePayload)
					fileName = fmt.Sprintf("dis_%s_did.csv", datePayload)
					diff := helpers.DaysBetween(helpers.Date(timeNow), helpers.Date(datePayload))
					content = ProcessDataNotDid(db, diff)
				} else {
					announcement = fmt.Sprintf("getting data disbursement %s", date)
					fileName = fmt.Sprintf("dis_%s.csv", date)
					content = ProcessDataNotDid(db, disbursementDay)
				}
			}

			if content == "" {
				rtm.SendMessage(rtm.NewOutgoingMessage("no disbursement for today", channel))
			} else {
				rtm.SendMessage(rtm.NewOutgoingMessage(announcement, channel))
			}
			_, err := client.UploadFile(slack.FileUploadParameters{
				Content:  content,
				Filetype: "csv",
				Filename: fileName,
				Channels: []string{channel},
			})
			if err != nil {
				panic(err)
			}
		},
	}
	return data, nil
}
