package modules

import (
	"fmt"
	"github.com/jinzhu/gorm"
)

func GetDataDid(db *gorm.DB, day uint64) ([]DataDisbursementDid, error) {
	query := fmt.Sprintf(`select
	b.id "branchId",
	b."name" "branchName",
	a."name" "areaName",
	b."bankAccountNo",
	sum(l.plafond) "amount",
	date(d."disbursementDate"),
	d.id "disbursementId",
	l.id "loanId",
	l.stage "stage",
	c2."name" "borrowerName",
	g."name" "majelis"
from
	disbursement d
join r_loan_disbursement rld on
	rld."disbursementId" = d.id
	and rld."deletedAt" isnull
join loan l on
	l.id = rld."loanId"
	and l."deletedAt" isnull
join r_loan_branch rlb on
	rlb."loanId" = l.id
	and rlb."deletedAt" isnull
join branch b on
	b.id = rlb."branchId"
	and b."deletedAt" isnull
left join r_area_branch rab on
	rab."branchId" = b.id
	and rab."deletedAt" isnull
left join area a on
	a.id = rab."areaId"
	and a."deletedAt" isnull
join r_loan_borrower rlb2 on
	rlb2."loanId"=l.id 
	and rlb2."deletedAt" is null 
join borrower b2 on 
	b2.id = rlb2."borrowerId" 
	and b2."deletedAt" is null
join r_cif_borrower rcb on
	rcb."borrowerId" = b2.id
	and rcb."deletedAt" is null
join cif c2 on
	c2.id=rcb."cifId" 
	and c2."deletedAt" is null
join r_loan_group rlg on 
	rlg."loanId" =l.id
	and rlg."deletedAt" is null
join "group" g on
	g.id = rlg."groupId" 
	and g."deletedAt" is null
where
	l."isUPK" = true
	and l."isLWK" = true
	and d."disbursementDate"::date = now()::date + %d::integer
	and l.stage not in ('FILING FAILED',
	'FILING-FAILED',
	'ARCHIVE',
	'ARCHIVE-REFUND',
	'DROPOUT',
	'DELETED',
	'TBA',
	'UNKNOWN',
	'DROPPING-FAILED',
	'INSTALLMENT',
	'REJECTED')
	and l."loanType" != 'NORMAL-ASTAR'
	and a.ID in ( 1,
	3,
	5,
	7,
	8,
	9,
	10,
	54,
	11,
	13,
	14,
	15,
	16,
	17,
	18,
	19,
	20,
	21,
	22,
	23,
	24,
	25,
	27,
	28,
	29,
	30,
	31,
	32,
	33,
	34,
	35,
	36,
	37,
	38,
	39,
	40,
	42,
	43,
	44,
	45,
	46,
	47,
	49,
	50,
	51,
	53,
	55,
	56,
	57,
	58,
	59,
	67,
	60,
	61,
	62,
	63,
	64,
	66,
	65,
	68,
	69,
	70,
	71,
	76,
	75,
	77,
	73,
	72,
	78,
	74,
	82,
	79,
	80,
	81,
	83,
	84,
	85,
	86,
	93,
	94,
	95,
	96,
	97,
	98,
	99,
	100,
	88,
	89,
	87,
	104,
	105,
	107,
	108,
	109,
	110,
	102,
	111,
	112,
	113,
	101,
	114,
	115,
	116,
	117,
	118,
	119,
	120,
	121,
	90,
	122,
	123,
	125)
group by
	l.id,
	b.id,
	date(d."disbursementDate"),
	a."name",
	d.id,
	c2."name",
	g."name";`, day)

	var data []DataDisbursementDid
	err := db.Raw(query).Scan(&data).Error
	if err != nil {
		return nil, err
	}
	fmt.Println(query)
	return data, nil
}


func GetDataNotDid(db *gorm.DB, day uint64) ([]DataDisbursementNotDid, error) {
	query := fmt.Sprintf(`select
	b.id "branchId",
	b."name" "branchName",
	a."name" "areaName",
	b."bankAccountNo",
	sum(l.plafond) "amount",
	date(d."disbursementDate")
from
	disbursement d
join r_loan_disbursement rld on
	rld."disbursementId" = d.id
	and rld."deletedAt" isnull
join loan l on
	l.id = rld."loanId"
	and l."deletedAt" isnull
join r_loan_branch rlb on
	rlb."loanId" = l.id
	and rlb."deletedAt" isnull
join branch b on
	b.id = rlb."branchId"
	and b."deletedAt" isnull
left join r_area_branch rab on
	rab."branchId" = b.id
	and rab."deletedAt" isnull
left join area a on
	a.id = rab."areaId"
	and a."deletedAt" isnull
where
	l."isUPK" = true
	and l."isLWK" = true
	and d."disbursementDate"::date = now()::date + %d::integer
	and l.stage not in ('FILING FAILED',
	'FILING-FAILED',
	'ARCHIVE',
	'ARCHIVE-REFUND',
	'DROPOUT',
	'DELETED',
	'TBA',
	'UNKNOWN',
	'DROPPING-FAILED',
	'INSTALLMENT',
	'REJECTED')
	and l."loanType" != 'NORMAL-ASTAR'
	and a.ID in (1,
	3,
	5,
	7,
	8,
	9,
	10,
	54,
	11,
	13,
	14,
	15,
	16,
	17,
	18,
	19,
	20,
	21,
	22,
	23,
	24,
	25,
	27,
	28,
	29,
	30,
	31,
	32,
	33,
	34,
	35,
	36,
	37,
	38,
	39,
	40,
	42,
	43,
	44,
	45,
	46,
	47,
	49,
	50,
	51,
	53,
	55,
	56,
	57,
	58,
	59,
	67,
	60,
	61,
	62,
	63,
	64,
	66,
	65,
	68,
	69,
	70,
	71,
	76,
	75,
	77,
	73,
	72,
	78,
	74,
	82,
	79,
	80,
	81,
	83,
	84,
	85,
	86,
	93,
	94,
	95,
	96,
	97,
	98,
	99,
	100,
	88,
	89,
	87,
	104,
	105,
	107,
	108,
	109,
	110,
	102,
	111,
	112,
	113,
	101,
	114,
	115,
	116,
	117,
	118,
	119,
	120,
	121,
	90,
	122,
	123,
	125)
group by
	b.id,
	date(d."disbursementDate"),
	a."name"`, day)

	var data []DataDisbursementNotDid
	err := db.Raw(query).Scan(&data).Error
	if err != nil {
		return nil, err
	}
	fmt.Println(query)
	return data, nil
}