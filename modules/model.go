package modules

type DataDisbursementDid struct {
	BranchID       uint64 `json:"branchId" gorm:"column:branchId"`
	BranchName     string `json:"branchName" gorm:"column:branchName"`
	AreaName       string `json:"areaName" gorm:"column:areaName"`
	BankAccountNo  string `json:"bankAccountNo" gorm:"column:bankAccountNo"`
	Amount         string `json:"amount" gorm:"column:amount"`
	Date           string `json:"date" gorm:"column:date"`
	DisbursementId uint64 `json:"disbursementId" gorm:"column:disbursementId"`
	LoanId         uint64 `json:"loanId" gorm:"column:loanId"`
	Stage          string `json:"stage" gorm:"column:stage"`
	BorrowerName   string `json:"borrowerName" gorm:"column:"borrowerName"`
	Majelis        string `json:"majelis" gorm:"column:"majelis"`
}

type DataDisbursementNotDid struct {
	BranchID      uint64 `json:"branchId" gorm:"column:branchId"`
	BranchName    string `json:"branchName" gorm:"column:branchName"`
	AreaName      string `json:"areaName" gorm:"column:areaName"`
	BankAccountNo string `json:"bankAccountNo" gorm:"column:bankAccountNo"`
	Amount        string `json:"amount" gorm:"column:amount"`
	Date          string `json:"date" gorm:"column:date"`
}
