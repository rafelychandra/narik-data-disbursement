package helpers

import (
	"encoding/csv"
	"fmt"
	"os"
	"reflect"
	"time"
)

func CreateCSV(data [][]string) (*os.File, error) {
	file, err := os.Create("result.csv")
	if err != nil {
		return nil, err
	}

	writer := csv.NewWriter(file)
	_ = writer.WriteAll(data)
	defer writer.Flush()
	defer file.Close()
	return file, nil
}

func ConvertToArrayString(input interface{}) [][]string {
	var records [][]string
	var header []string // The first record in records will contain the names of the fields
	object := reflect.ValueOf(input)

	// The first record in the records slice should contain headers / field names
	if object.Len() > 0 {
		first := object.Index(0)
		typ := first.Type()

		for i := 0; i < first.NumField(); i++ {
			field, ok := reflect.TypeOf(input).Elem().FieldByName(typ.Field(i).Name)
			if !ok {
				panic("Field not found")
			}

			header = append(header, field.Tag.Get("json"))
		}
		records = append(records, header)
	}

	// Make a slice of objects to iterate through & populate the string slice
	var items []interface{}
	for i := 0; i < object.Len(); i++ {
		items = append(items, object.Index(i).Interface())
	}

	// Populate the rest of the items into <records>
	for _, v := range items {
		item := reflect.ValueOf(v)
		var record []string
		for i := 0; i < item.NumField(); i++ {
			itm := item.Field(i).Interface()
			record = append(record, fmt.Sprintf("%v", itm))
		}
		records = append(records, record)
	}
	return records
}

func DaysBetween(a, b time.Time) uint64 {
	if a.After(b) {
		a, b = b, a
	}

	days := -a.YearDay()
	for year := a.Year(); year < b.Year(); year++ {
		days += time.Date(year, time.December, 31, 0, 0, 0, 0, time.UTC).YearDay()
	}
	days += b.YearDay()

	return uint64(days)
}

func Date(s string) time.Time {
	d, _ := time.Parse("2006-01-02", s)
	return d
}
