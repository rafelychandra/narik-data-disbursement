package main

import (
	"context"
	"fmt"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/joho/godotenv"
	"narikData/config"
	"narikData/modules"
)

func main() {
	err := godotenv.Load(".env")
	if err != nil {
		panic(err)
	}

	db, err := config.ConnectionDatabase()
	if err != nil {
		panic(err)
	}
	defer db.Close()

	bot := config.ConnectionSlack()
	fmt.Println("CONNECTION SUCCESS")
	dataCSV, err := modules.CallBot(db)
	if err != nil {
		panic(err)
	}
	bot.Command("data <payload> <time>", dataCSV)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	err = bot.Listen(ctx)
	if err != nil {
		panic(err)
	}
}
